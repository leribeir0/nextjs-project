
### Este é o repositório Git da aplicação DevNews! desenvolvida durante o curso.

## Rodando a aplicação no seu PC

Faça um clone deste repositório e instale no seu ambiente de desenvolvimento usando o seguinte comando no seu terminal (escolha um diretório apropriado):

```shell
git clone https://github.com/leribeir0/nextjs-project.git
```

Após clonar o conteúdo do repositório, acesse o diretório criado e efetue a instalação das dependências:

```shell
cd nextjs-project

npm install
```

Após essa instalação execute a aplicação com o comando `npm run dev`. A aplicação estará disponível no endereço `http://localhost:3000`.
